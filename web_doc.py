import urllib.request as url_request

class Robot():
    def __init__(self, _url,):
        self.url = _url
        self.descargado = False
        self.content

    def retrieve(self):
        if not self.descargado:
            print(f"\n-----------DESCARGANDO URL: {self.url}-----------\n")
            file = url_request.urlopen(self.url)
            self._content = file.read().decode('utf-8')
            self.descargado = True
        else:
            print("\n-----------URL DESCARGADA PREVIAMENTE-----------\n")


    def show(self):
        print(f"\n-----------EL CONTENIDO DE LA URL {self.url} ES: -----------\n{self.content()}\n")
    def content(self):
        self.retrieve()
        return self._content

class Cache():
    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        if url not in self.cache:
            robot_c = Robot(url)
            self.cache[url] = robot_c
            robot_c.retrieve()

    def show(self, url):
        print(f"\n-----------EL CONTENIDO DE LA URL {url} ES: -----------\n{self.content(url)}\n")

    def show_all(self):
        urls = list(self.cache.keys())
        for url in urls:
            print(f"\n\n-----------EL CONTENIDO DE LA URL {url} ES:-----------\n{self.content(url)}\n")

    def content(self, url):
        self.retrieve(url)
        return self.cache[url]._content
        pass
if __name__ == "__main__":

    print("-----------DESCARGA DE DATOS WEB CON ROBOT Y CACHE-----------\n\n")
    print("-----------PROBANDO CLASE ROBOT-----------\n")
    r = Robot('http://gsyc.urjc.es/')
    print(f"\n-----------ROBOT INSTANCIADO CON URL: {r.url}-----------\n")
    r.show()
    r.retrieve()
    r.retrieve()
    print("-----------PROBANDO CLASE CACHE-----------")
    c = Cache()
    c.retrieve('http://gsyc.urjc.es/')
    c.show('https://www.aulavirtual.urjc.es')
    c.show_all()